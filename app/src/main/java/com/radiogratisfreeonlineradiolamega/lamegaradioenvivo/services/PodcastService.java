package com.radiogratisfreeonlineradiolamega.lamegaradioenvivo.services;

import com.radiogratisfreeonlineradiolamega.lamegaradioenvivo.models.Podcast;
import com.radiogratisfreeonlineradiolamega.lamegaradioenvivo.restApi.RestfulApiConstants;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Created by alex on 19/01/18.
 */

public interface PodcastService {
    @GET(RestfulApiConstants.PODCASTS_URL)
    Call<List<Podcast>> getJsonArrayData(@Query("page") String page, @Header("Authorization") String credentials);
}
