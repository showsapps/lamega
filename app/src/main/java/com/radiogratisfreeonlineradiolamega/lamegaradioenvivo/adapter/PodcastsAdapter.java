package com.radiogratisfreeonlineradiolamega.lamegaradioenvivo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.radiogratisfreeonlineradiolamega.R;
import com.radiogratisfreeonlineradiolamega.lamegaradioenvivo.models.Podcast;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;
import java.util.List;

/**
 * Created by alex on 17/12/17.
 */

public class PodcastsAdapter extends RecyclerView.Adapter {
    // ... view holder defined above...
    // Store a member variable for the contacts
    private List<Podcast> mPodcasts;
    // Store the context for easy access
    private Context mContext;
    // Define listener member variable
    private OnItemClickListener listener;
    boolean value;

    private final int ITEM_VIEW_TYPE_BASIC = 0;
    private final int ITEM_VIEW_TYPE_FOOTER = 1;
    private final int AD_TYPE = 2;

    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }
    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    // Pass in the contact array into the constructor
    public PodcastsAdapter(Context context, List<Podcast> podcasts) {
        mPodcasts = podcasts;
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        if (viewType == ITEM_VIEW_TYPE_BASIC) {
            // Inflate the custom layout
            View podcastView = inflater.inflate(R.layout.item_podcast, parent, false);
            // Return a new holder instance
            viewHolder = new ViewHolder(podcastView);
        } else{
            if(viewType == AD_TYPE)
            {
                LinearLayout adContainer = new LinearLayout(context);
                AdView adView = new AdView(context);
                adView.setAdSize(AdSize.BANNER);
                adView.setAdUnitId(parent.getResources().getString(R.string.banner_ad_unit_id));

                // Initiate a generic request to load it with an ad
                AdRequest adRequest = new AdRequest.Builder().build();
                adView.loadAd(adRequest);

                // Place the ad view.
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                params.gravity = Gravity.CENTER;
                adView.getPaddingLeft();

                adContainer.addView(adView, params);
                viewHolder = new LayoutAdviewHolder(adContainer);
            }else{
            View podcastView = inflater.inflate(R.layout.layout_loading_item, parent, false);
            viewHolder = new ProgressViewHolder(podcastView);
            }
        }
        return viewHolder;
    }

    public void refreshAdapter(boolean value, List<Podcast> tempPodcast){
        this.value = value;
        this.mPodcasts = tempPodcast;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            // Get the data model based on position
            Podcast podcast = mPodcasts.get(position);

            // Set item views based on your views and data model
            TextView textViewTitle = ((ViewHolder) holder).titleTextView;
            textViewTitle.setText(podcast.getTitle());
            TextView textViewDesc = ((ViewHolder) holder).descTextView;
            textViewDesc.setText(podcast.getDesc());
            TextView textViewDuration = ((ViewHolder) holder).durationTextView;
            textViewDuration.setText(podcast.getDuration());
            String img_url = podcast.getImageUrl();

            Picasso.with(mContext)
                    .load(img_url)
                    .placeholder(R.drawable.placeholder)
                    .into(((ViewHolder) holder).podcastImageView);
        }else if (holder instanceof ProgressViewHolder) {
            //check data completion
            //if value is true, change the visibility of progressbar to gone to identify the user
            if (!value) {
                ((ProgressViewHolder)holder).progressBar.setVisibility(
                        View.VISIBLE);
                ((ProgressViewHolder)holder).progressBar.setIndeterminate(true);
            } else ((ProgressViewHolder)holder).progressBar.setVisibility(
                    View.GONE);
        }
    }


    @Override
    public int getItemCount() {
        return mPodcasts.size();
    }

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView titleTextView;
        public TextView descTextView;
        public TextView durationTextView;
        public ImageView podcastImageView;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview

        public ViewHolder(final View itemView) {
            super(itemView);
            this.titleTextView = (TextView) itemView.findViewById(R.id.txt_view_podcast_title);
            this.descTextView = (TextView) itemView.findViewById(R.id.txt_view_podcast_desc);
            this.durationTextView = (TextView) itemView.findViewById(R.id.txt_view_podcast_duration);
            this.podcastImageView = (ImageView) itemView.findViewById(R.id.imageview_podcast_item);

            // Setup the click listener
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Triggers click upwards to the adapter on click
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(itemView, position);
                        }
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        //check for the pre-defined value that will indicate footer
        if(mPodcasts.get(position) != null) {
            if (mPodcasts.get(position).getTitle() == "inlineAd"){
                return AD_TYPE;}
            else
                return ITEM_VIEW_TYPE_BASIC;
        }else
            return ITEM_VIEW_TYPE_FOOTER;
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }

    private class LayoutAdviewHolder extends RecyclerView.ViewHolder {
        public LayoutAdviewHolder(View v) {
            super(v);
        }
    }
}
