package com.radiogratisfreeonlineradiolamega.lamegaradioenvivo;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.radiogratisfreeonlineradiolamega.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.ohoussein.playpause.PlayPauseView;
import com.squareup.picasso.Picasso;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PodcastPlayerActivity extends AppCompatActivity {

    private MediaPlayerService player;
    boolean serviceBound = false;
    String urlAudio;
    String urlAudioWithToken;
    private AdView mAdView;
    //AudioPlayer notification ID
    private static final int NOTIFICATION_ID = 101;
    String title = "";
    String desc = "";
    String id ="";
    String segmentName = "";
    String podcastImageUrl ="";
    LocalBroadcastManager mLocalBroadcastManager;
    String playerStatus;
    String mediaType;
    String mediaId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_podcast_player);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final PlayPauseView playerView = (PlayPauseView) findViewById(R.id.podcast_play_pause_view);

        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        segmentName = intent.getStringExtra("segmentName");
        title = intent.getStringExtra("title");
        desc = intent.getStringExtra("desc");
        String token = md5(title.replace(" ", ""));
        urlAudio = intent.getStringExtra("url");
        podcastImageUrl = intent.getStringExtra("imageUrl");
        urlAudioWithToken = urlAudio + "?serial=" + token;

        getSupportActionBar().setTitle(segmentName);

        mAdView = (AdView) findViewById(R.id.adViewPodcastPlayer);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        TextView podcast_title = (TextView)findViewById(R.id.textView_title_podcastplayer);
        podcast_title.setText(title);
        ImageView podcast_image_view = (ImageView)findViewById(R.id.ivCoverPodcast);
        TextView podcast_desc = (TextView)findViewById(R.id.txv_podcast_desc);
        podcast_desc.setText(desc);
        TextView segment_name_textview = (TextView)findViewById(R.id.textview_segment_name);
        segment_name_textview.setText(segmentName);

        Picasso.with(getApplicationContext())
                .load(podcastImageUrl)
                .placeholder(R.drawable.placeholder)
                .into(podcast_image_view);

        playerStatus = ((MyApplication) this.getApplication()).getplayerStatus();
        mediaType = ((MyApplication) this.getApplication()).getmediaType();
        mediaId = ((MyApplication) this.getApplication()).getMediaId();

        if (playerStatus == "PLAYING" && mediaType.equals("PODCAST") && id.equals(mediaId))
            playerView.change(false);

        playerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playerView.isPlay()) {
                    playAudio(urlAudioWithToken);
                }
                else{
                    if (serviceBound)
                        stopAudio();
                    else{
                        Intent playerIntentForStop = new Intent(getApplicationContext(), MediaPlayerService.class);
                        playerIntentForStop.setAction("PAUSE");
                        startService(playerIntentForStop);
                        bindService(playerIntentForStop, serviceConnection, Context.BIND_AUTO_CREATE);
                    }
                }
                playerView.toggle();
            }
        });

        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction("com.radiogratisfreeonlineradiolamega.lamegaradioenvivo.action.close");
        mLocalBroadcastManager.registerReceiver(mBroadcastReceiver, mIntentFilter);
    }

    //Binding this Client to the AudioPlayer Service
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            MediaPlayerService.LocalBinder binder = (MediaPlayerService.LocalBinder) service;
            player = binder.getService();
            serviceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            serviceBound = false;
        }
    };

    private void playAudio(String media) {
        //Check is service is active
        if (!serviceBound){
            Intent playerIntent = new Intent(this, MediaPlayerService.class);
            playerIntent.setAction("PLAY");
            playerIntent.putExtra("id", id);
            playerIntent.putExtra("media", media);
            playerIntent.putExtra("contentText", getResources().getString(R.string.NotificationPlayerSubtitle));
            playerIntent.putExtra("contentTitle", title);
            playerIntent.putExtra("contentInfo", segmentName);
            playerIntent.putExtra("mediaType","PODCAST");
            startService(playerIntent);
            bindService(playerIntent, serviceConnection, Context.BIND_AUTO_CREATE);
        } else {
            //Service is active
            //Send media with BroadcastReceiver
            player.resumeAudio();
            //Intent broadcastIntent = new Intent(Broadcast_PLAY_NEW_AUDIO);
            //sendBroadcast(broadcastIntent);
        }
    }

    private void stopAudio() {
        if (serviceBound) {
            player.pauseAudio();
            //removeNotification();
            //unbindService(serviceConnection);
            //service is active
            //player.stopSelf();
            //serviceBound = false;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean("ServiceState", serviceBound);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        serviceBound = savedInstanceState.getBoolean("ServiceState");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals("com.radiogratisfreeonlineradiolamega.lamegaradioenvivo.action.close")){
                finish();
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (serviceBound) {
            unbindService(serviceConnection);
        }
        mLocalBroadcastManager.unregisterReceiver(mBroadcastReceiver);

    }

    public String md5(String s) {
        MessageDigest m = null;

        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        m.update(s.getBytes(),0,s.length());
        String hash = new BigInteger(1, m.digest()).toString(16);
        return hash;
    }


    private void removeNotification() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);
    }
}
