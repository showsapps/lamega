package com.radiogratisfreeonlineradiolamega.lamegaradioenvivo;

/**
 * Created by alex on 19/11/17.
 */

public enum PlaybackStatus {
    PLAYING,
    PAUSED,
    STOPPED
}
