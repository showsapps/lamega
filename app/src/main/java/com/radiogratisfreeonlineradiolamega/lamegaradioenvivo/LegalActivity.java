package com.radiogratisfreeonlineradiolamega.lamegaradioenvivo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.radiogratisfreeonlineradiolamega.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class LegalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legal);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Política de Privacidad");

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
