package com.radiogratisfreeonlineradiolamega.lamegaradioenvivo;

/**
 * Created by alex on 16/12/17.
 */

public enum PlayerServiceStatus {
    RUNNING,
    STOPPED
}
