package com.radiogratisfreeonlineradiolamega.lamegaradioenvivo;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;

import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.session.MediaSessionManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.radiogratisfreeonlineradiolamega.R;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;


import static android.content.ContentValues.TAG;

/**
 * Created by alex on 2/9/18.
 */

public class MediaPlayerService extends Service implements Player.EventListener, AudioManager.OnAudioFocusChangeListener {
    private Handler mainHandler;
    private RenderersFactory renderersFactory;
    private BandwidthMeter bandwidthMeter;
    private LoadControl loadControl;
    private DataSource.Factory dataSourceFactory;
    private ExtractorsFactory extractorsFactory;
    private MediaSource mediaSource;
    private TrackSelection.Factory trackSelectionFactory;
    private SimpleExoPlayer player;
    private TrackSelector trackSelector;
    private String mediaFile;
    String mediaOld;
    String contentText = "";
    String contentTitle = "";
    String contentInfo = "";
    String mediaType = "";
    String mediaId;
    String CHANNEL_ID;

    public static final String ACTION_PLAY = "com.radiogratisfreeonlineradiolamega.lamegaradioenvivo.ACTION_PLAY";
    public static final String ACTION_PAUSE = "com.radiogratisfreeonlineradiolamega.lamegaradioenvivo.ACTION_PAUSE";
    public static final String ACTION_PREVIOUS = "com.radiogratisfreeonlineradiolamega.lamegaradioenvivo.ACTION_PREVIOUS";
    public static final String ACTION_NEXT = "com.radiogratisfreeonlineradiolamega.lamegaradioenvivo.ACTION_NEXT";
    public static final String ACTION_STOP = "com.radiogratisfreeonlineradiolamega.lamegaradioenvivo.ACTION_STOP";
    PlaybackStatus playerStatus = PlaybackStatus.STOPPED;
    PlaybackStatus previousStatus = PlaybackStatus.STOPPED;
    //MediaSession
    private MediaSessionManager mediaSessionManager;
    private MediaSessionCompat mediaSession;
    private MediaControllerCompat.TransportControls transportControls;

    //AudioPlayer notification ID
    private static final int NOTIFICATION_ID = 101;
    private Notification notification;
    //Handle incoming phone calls
    private boolean ongoingCall = false;
    private PhoneStateListener phoneStateListener;
    private TelephonyManager telephonyManager;
    //Used to pause/resume MediaPlayer
    private AudioManager audioManager;

    public MediaPlayerService() {
    }


    @Override
    public void onCreate() {
        ((MyApplication) this.getApplication()).setplayerStatus("STOPPED");
        Log.d("ERAZNO", "Creating Service...");
        // Perform one-time setup procedures
        // Manage incoming phone calls during playback.
        // Pause MediaPlayer on incoming call,
        // Resume on hangup.
        callStateListener();
        //ACTION_AUDIO_BECOMING_NOISY -- change in audio outputs -- BroadcastReceiver
        registerBecomingNoisyReceiver();
    }

    private void initMediaPlayer() {
        renderersFactory = new DefaultRenderersFactory(getApplicationContext());
        bandwidthMeter = new DefaultBandwidthMeter();
        trackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        trackSelector = new DefaultTrackSelector(trackSelectionFactory);
        loadControl = new DefaultLoadControl();

        player = ExoPlayerFactory.newSimpleInstance(renderersFactory, trackSelector, loadControl);
        player.addListener(this);

        dataSourceFactory = new DefaultDataSourceFactory(getApplicationContext(), "ExoplayerDemo");
        extractorsFactory = new DefaultExtractorsFactory();
        mainHandler = new Handler();
        mediaSource = new ExtractorMediaSource(Uri.parse(mediaFile),
                dataSourceFactory,
                extractorsFactory,
                mainHandler,
                null);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("MANDRIL", "Starting service with intent " + intent.getAction() + " and action " + playerStatus);

        if (intent.getAction() == "CLOSE"){
            close();
        }else {
            if (mediaOld != mediaFile && playerStatus == PlaybackStatus.PLAYING) {
                pauseAudio();
                playerStatus = PlaybackStatus.PAUSED;
            }

            if (intent.getAction() == "PAUSE") {
                pauseAudio();
                buildNotification(PlaybackStatus.PAUSED);
            } else if (intent.getAction() == "PLAY" && playerStatus != PlaybackStatus.PLAYING && requestAudioFocus()) {
                if (intent.getExtras() != null) {
                    //An audio file is passed to the service through putExtra();
                    mediaOld = mediaFile;
                    mediaId = intent.getExtras().getString("id");
                    mediaFile = intent.getExtras().getString("media");
                    contentText = intent.getExtras().getString("contentText");
                    contentTitle = intent.getExtras().getString("contentTitle");
                    contentInfo = intent.getExtras().getString("contentInfo");
                    mediaType = intent.getExtras().getString("mediaType");
                    if (mediaType != null)
                        ((MyApplication) this.getApplication()).setmediaType(mediaType);
                    if (mediaId != null)
                        ((MyApplication) this.getApplication()).setMediaId(mediaId);
                }
                initMediaPlayer();
                player.prepare(mediaSource);
                resumeAudio();
                try {
                    initMediaSession();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                buildNotification(PlaybackStatus.PLAYING);
                startForeground(NOTIFICATION_ID, notification);
            }

            //Handle Intent action from MediaSession.TransportControls
            handleIncomingActions(intent);
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "Destroying service...");
        player.setPlayWhenReady(false);
        stopForeground(true);

        if (player != null) {
            pauseAudio();
            player.release();
        }
        if (audioManager != null)
            removeAudioFocus();
        //Disable the PhoneStateListener
        if (phoneStateListener != null) {
            telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
        }

        removeNotification();
        stopSelf();

        //unregister BroadcastReceivers
        unregisterReceiver(becomingNoisyReceiver);
        //unregisterReceiver(playNewAudio);

        //clear cached playlist
        //new StorageUtil(getApplicationContext()).clearCachedAudioPlaylist();

    }

    private void close(){
        stopForeground(true);
        pauseAudio();
        stopSelf();
        removeNotification();
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
                .getInstance(MediaPlayerService.this);
        localBroadcastManager.sendBroadcast(new Intent(
                "com.radiogratisfreeonlineradiolamega.lamegaradioenvivo.action.close"));
        player.release();
    }

    public void resumeAudio() {
        previousStatus = playerStatus;
        if(playerStatus != PlaybackStatus.PLAYING){
            player.setPlayWhenReady(true);
            ((MyApplication) this.getApplication()).setplayerStatus("PLAYING");
            playerStatus = PlaybackStatus.PLAYING;
        }
    }

    public void pauseAudio() {
        previousStatus = playerStatus;
        player.setPlayWhenReady(false);
        ((MyApplication) this.getApplication()).setplayerStatus("PAUSED");
        playerStatus = PlaybackStatus.PAUSED;
        if(player != null)
            buildNotification(PlaybackStatus.PAUSED);
    }

    private void initMediaSession() throws RemoteException {
        if (mediaSessionManager != null) return; //mediaSessionManager exists

        mediaSessionManager = (MediaSessionManager) getSystemService(Context.MEDIA_SESSION_SERVICE);
        // Create a new MediaSession
        mediaSession = new MediaSessionCompat(getApplicationContext(), "AudioPlayer");
        //Get MediaSessions transport controls
        transportControls = mediaSession.getController().getTransportControls();
        //set MediaSession -> ready to receive media commands
        mediaSession.setActive(true);
        //indicate that the MediaSession handles transport control commands
        // through its MediaSessionCompat.Callback.
        mediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);

        // Attach Callback to receive MediaSession updates
        mediaSession.setCallback(new MediaSessionCompat.Callback() {

            // Implement callbacks
            @Override
            public void onPlay() {
                super.onPlay();
                resumeAudio();
                buildNotification(PlaybackStatus.PLAYING);
            }

            @Override
            public void onPause() {
                super.onPause();
                pauseAudio();
                buildNotification(PlaybackStatus.PAUSED);
            }

            @Override
            public void onStop() {
                super.onStop();
                removeNotification();
                //Stop the service
                stopSelf();
            }

            @Override
            public void onSeekTo(long position) {
                super.onSeekTo(position);
            }
        });
    }

    private void handleIncomingActions(Intent playbackAction) {
        if (playbackAction == null || playbackAction.getAction() == null) return;

        String actionString = playbackAction.getAction();
        if (actionString.equalsIgnoreCase(ACTION_PLAY)) {
            transportControls.play();
        } else if (actionString.equalsIgnoreCase(ACTION_PAUSE)) {
            transportControls.pause();
        } else if (actionString.equalsIgnoreCase(ACTION_NEXT)) {
            transportControls.skipToNext();
        } else if (actionString.equalsIgnoreCase(ACTION_PREVIOUS)) {
            transportControls.skipToPrevious();
        } else if (actionString.equalsIgnoreCase(ACTION_STOP)) {
            transportControls.stop();
        }
    }

    private  void buildNotification(PlaybackStatus playbackStatus) {
        int notificationAction = android.R.drawable.ic_media_pause;//needs to be initialized
        PendingIntent play_pauseAction = null;
        //Build a new notification according to the current state of the MediaPlayer
        if (playbackStatus == PlaybackStatus.PLAYING) {
            notificationAction = android.R.drawable.ic_media_pause;
            // using for checking when gain and loss focus by a
            playerStatus = PlaybackStatus.PLAYING;
            //create the pause action
            play_pauseAction = playbackAction(1);
        } else if (playbackStatus == PlaybackStatus.PAUSED) {
            notificationAction = android.R.drawable.ic_media_play;
            // using for checking when gain and loss focus by a
            playerStatus = PlaybackStatus.PAUSED;
            //create the play action
            play_pauseAction = playbackAction(0);
        }

        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(),
                R.drawable.la_mega_radio); //replace with your own image

        Intent nid = new Intent(getApplicationContext(), MainActivity.class);
        // If you were starting a service, you wouldn't using getActivity() here
        PendingIntent ci = PendingIntent.getActivity(getApplicationContext(), NOTIFICATION_ID, nid, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent stopSelf = new Intent(getApplicationContext(), MediaPlayerService.class);
        stopSelf.setAction("CLOSE");
        PendingIntent pStopSelf = PendingIntent.getService(this, NOTIFICATION_ID, stopSelf, PendingIntent.FLAG_CANCEL_CURRENT);

        // Create a new Notification
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            CHANNEL_ID = "my_channel_01";
            CharSequence name = "my_channel";
            String Description = "This is my channel";
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(Description);
            mChannel.enableLights(false);
            mChannel.enableVibration(false);
            mChannel.setShowBadge(false);
            notificationManager.createNotificationChannel(mChannel);
        }

        android.support.v4.app.NotificationCompat.Builder notificationBuilder = new android.support.v4.app.NotificationCompat.Builder(this, CHANNEL_ID);

        notificationBuilder.setShowWhen(false)
                // Set the Notification style
                .setStyle(new android.support.v4.media.app.NotificationCompat.MediaStyle()
                        // Attach our MediaSession token
                        .setMediaSession(mediaSession.getSessionToken())
                        // Show our playback controls in the compact notification view.
                        .setShowActionsInCompactView(0, 1))
                // Set the Notification color
                .setColor(getResources().getColor(R.color.colorPrimary))
                // Set the large and small icons
                .setLargeIcon(largeIcon)
                // For dismisable notification
                //.setOngoing(true)
                .setSmallIcon(R.drawable.ic_radio_white)
                // Set Notification content information
                .setContentText(contentText)
                .setContentTitle(contentTitle)
                .setContentInfo(contentInfo)
                .setContentIntent(ci)
                // Add playback actions
                // .addAction(android.R.drawable.ic_media_previous, "previous", playbackAction(3))
                .addAction(notificationAction, "pause", play_pauseAction)
                .addAction(android.R.drawable.ic_menu_close_clear_cancel, "close", pStopSelf);
        // .addAction(android.R.drawable.ic_media_next, "next", playbackAction(2));
        notification = notificationBuilder.build();
        notificationManager.notify(NOTIFICATION_ID, notification);
    }


    private void removeNotification() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);
    }

    private PendingIntent playbackAction(int actionNumber) {
        Intent playbackAction = new Intent(this, MediaPlayerService.class);
        switch (actionNumber) {
            case 0:
                // Play
                playbackAction.setAction(ACTION_PLAY);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            case 1:
                // Pause
                playbackAction.setAction(ACTION_PAUSE);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            case 2:
                // Next track
                playbackAction.setAction(ACTION_NEXT);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            case 3:
                // Previous track
                playbackAction.setAction(ACTION_PREVIOUS);
                return PendingIntent.getService(this, actionNumber, playbackAction, 0);
            default:
                break;
        }
        return null;
    }

    // Binder given to clients
    private final IBinder iBinder = new LocalBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return iBinder;
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    public void raw_ffwd(int action){
        if (player != null){
            long pos = player.getCurrentPosition();
            if(action == 2)
                player.seekTo( pos - 30000);
            else
                player.seekTo(pos + 30000);
        }
    }

    public class LocalBinder extends Binder {
        public MediaPlayerService getService() {
            return MediaPlayerService.this;
        }
    }

    private boolean requestAudioFocus() {
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int result = audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            //Focus gained
            return true;
        }
        //Could not gain focus
        return false;
    }

    //Becoming noisy
    private BroadcastReceiver becomingNoisyReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //pause audio on ACTION_AUDIO_BECOMING_NOISY
            pauseAudio();
            buildNotification(PlaybackStatus.PAUSED);
        }
    };

    private void registerBecomingNoisyReceiver() {
        //register after getting audio focus
        IntentFilter intentFilter = new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
        registerReceiver(becomingNoisyReceiver, intentFilter);
    }

    @Override
    public void onAudioFocusChange(int focusState) {
        //Invoked when the audio focus of the system is updated.
        switch (focusState) {
            case AudioManager.AUDIOFOCUS_GAIN:
                // resume playback
                if (player == null) initMediaPlayer();
                else if (playerStatus != PlaybackStatus.PLAYING && previousStatus == PlaybackStatus.PLAYING) resumeAudio();
                player.setVolume(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                // Lost focus for an unbounded amount of time: stop playback and release media player
                if (playerStatus == PlaybackStatus.PLAYING)pauseAudio();
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                // Lost focus for a short time, but we have to stop
                // playback. We don't release the media player because playback
                // is likely to resume
                if (playerStatus == PlaybackStatus.PLAYING){
                    previousStatus = PlaybackStatus.PLAYING;
                    pauseAudio();
                }
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                // Lost focus for a short time, but it's ok to keep playing
                // at an attenuated level
                if (playerStatus == PlaybackStatus.PLAYING){
                    previousStatus = PlaybackStatus.PLAYING;
                    player.setVolume(0.1f);
                }
                break;
        }
    }


    //Handle incoming phone calls
    private void callStateListener() {


        // Get the telephony manager
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        //Starting listening for PhoneState changes
        phoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                switch (state) {
                    //if at least one call exists or the phone is ringing
                    //pause the MediaPlayer
                    case TelephonyManager.CALL_STATE_OFFHOOK:
                    case TelephonyManager.CALL_STATE_RINGING:
                        if (player != null) {
                            if (playerStatus == PlaybackStatus.PLAYING) {
                                previousStatus = PlaybackStatus.PLAYING;
                                pauseAudio();
                            }
                            ongoingCall = true;
                        }
                        break;
                    case TelephonyManager.CALL_STATE_IDLE:
                        // Phone idle. Start playing.
                        if (player != null) {
                            if (ongoingCall) {
                                ongoingCall = false;
                                if (playerStatus != PlaybackStatus.PLAYING && previousStatus == PlaybackStatus.PLAYING){
                                    resumeAudio();
                                    buildNotification(PlaybackStatus.PLAYING);
                                }

                            }
                        }
                        break;
                }
            }
        };
        // Register the listener with the telephony manager
        // Listen for changes to the device call state.
        telephonyManager.listen(phoneStateListener,
                PhoneStateListener.LISTEN_CALL_STATE);
    }

    private boolean removeAudioFocus() {
        return AudioManager.AUDIOFOCUS_REQUEST_GRANTED ==
                audioManager.abandonAudioFocus(this);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(NOTIFICATION_ID);
    }
}