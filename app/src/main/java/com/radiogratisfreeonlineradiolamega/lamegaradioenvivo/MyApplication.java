package com.radiogratisfreeonlineradiolamega.lamegaradioenvivo;

import android.app.Application;

/**
 * Created by alex on 27/12/17.
 */

public class MyApplication extends Application {
    private String playerStatus;
    private String mediaType;
    private String mediaId;

    public String getplayerStatus() {
        return playerStatus;
    }

    public void setplayerStatus(String playerStatus) {
        this.playerStatus = playerStatus;
    }

    public void setmediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getmediaType() {
        return mediaType;
    }

    public String getMediaId() {return mediaId;}

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }
}
