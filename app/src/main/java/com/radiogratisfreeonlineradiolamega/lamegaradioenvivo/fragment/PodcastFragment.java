package com.radiogratisfreeonlineradiolamega.lamegaradioenvivo.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.radiogratisfreeonlineradiolamega.lamegaradioenvivo.models.Podcast;
import com.radiogratisfreeonlineradiolamega.lamegaradioenvivo.services.PodcastService;
import com.radiogratisfreeonlineradiolamega.lamegaradioenvivo.PodcastPlayerActivity;
import com.radiogratisfreeonlineradiolamega.lamegaradioenvivo.adapter.PodcastsAdapter;
import com.radiogratisfreeonlineradiolamega.R;
import com.radiogratisfreeonlineradiolamega.lamegaradioenvivo.restApi.RestfulApiConstants;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.radiogratisfreeonlineradiolamega.lamegaradioenvivo.SslAcept.getSlrClubClient;

/**
 * Created by alex on 28/01/18.
 */

public class PodcastFragment extends Fragment {

    RecyclerView rvPodcasts;
    RecyclerView.LayoutManager mLayoutManager;
    PodcastsAdapter adapter;
    ArrayList<Podcast> podcasts = new ArrayList<>();
    ProgressBar firstLoadingBar;
    int page = 1;
    int totalItemCount,lastVisibleItem,firstVisibleItem;
    boolean isLoading;
    private int visibleThreshold = 2;
    private InterstitialAd mInterstitialAd;
    String id;
    String title;
    String url;
    String desc;
    String imageUrl;
    String segmentName;
    ProgressDialog loadingDialog;
    Intent podcastPlayerIntent;

    public PodcastFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_podcast, container, false);

        firstLoadingBar = (ProgressBar) v.findViewById(R.id.firstLoadingBar);
        rvPodcasts = (RecyclerView) v.findViewById(R.id.rvPodcastList);
        mLayoutManager = new LinearLayoutManager(getContext());
        rvPodcasts.setLayoutManager(mLayoutManager);

        segmentName = getResources().getString(R.string.segment_name_podcast);

        // add on scrolling logic
        rvPodcasts.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);
                // total number of items in the data set held by the adapter
                totalItemCount = mLayoutManager.getItemCount();
                //adapter position of the first visible view.
                lastVisibleItem = ((LinearLayoutManager)recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                firstVisibleItem = ((LinearLayoutManager)recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

                //if it need to reload some more data, execute loadData()
                if (!isLoading && totalItemCount <=
                        (lastVisibleItem+visibleThreshold)) {
                    try {
                        loadMoreData();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    isLoading = true;
                }

            }
        });

        // Get data an set adapter
        try {
            setPodcastsDataFirstTime(Integer.toString(page));
        } catch (Exception e) {
            e.printStackTrace();
        }


        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mLayoutManager = new LinearLayoutManager(getContext());
        rvPodcasts.setLayoutManager(mLayoutManager);

        loadingDialog = new ProgressDialog(getContext());
        loadingDialog.setTitle(getResources().getString(R.string.internal_app_name));
        loadingDialog.setMessage(getResources().getString(R.string.initial_loading_desc));
        loadingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        loadingDialog.setCancelable(false);
        loadingDialog.setCanceledOnTouchOutside(false);
        loadingDialog.show();

        mInterstitialAd = new InterstitialAd(getContext());
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.interstitial_ad_unit_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());


        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                loadAd(); // Need to reload the Ad when it is closed.
                setupIntent();
                getActivity().startActivity(podcastPlayerIntent);
            }
            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                Log.e("Ads","Fail interstitial To load ad ERROR " + errorCode);
                setupIntent();
                getActivity().startActivity(podcastPlayerIntent);
            }
        });
    }

    private void loadMoreData() throws Exception {
        page = page + 1;
        //add null for loading
        podcasts.add(null);
        //notify inserted item null
        adapter.notifyItemInserted(podcasts.size() - 1);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestfulApiConstants.BASE_URL)
                .client(getSlrClubClient(getContext()))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PodcastService service = retrofit.create(PodcastService.class);
        Call<List<Podcast>> call = service.getJsonArrayData(Integer.toString(page), RestfulApiConstants.TOKEN);
        call.enqueue(new Callback<List<Podcast>>() {
            @Override
            public void onResponse(Call<List<Podcast>> call, Response<List<Podcast>> response) {
                //remove previously added null for loading
                podcasts.remove(podcasts.size() - 1);
                //notify removed item
                adapter.notifyItemRemoved(podcasts.size());
                // set scroll like continuning page
                mLayoutManager.scrollToPosition(firstVisibleItem + 1);

                try{
                    int numberItemsReponsed = response.body().size();

                    for(int i = 0; i < numberItemsReponsed; i++){
                        if (i % 4 == 0){
                            Podcast podForAd = new Podcast();
                            podForAd.setTitle("inlineAd");
                            podcasts.add(podForAd);
                        }
                        podcasts.add(response.body().get(i));
                    }
                    // if not more data in api, disabled load more
                    if(numberItemsReponsed == 0)
                        visibleThreshold = 0;

                    adapter = new PodcastsAdapter(getContext(),podcasts);
                    adapter.setOnItemClickListener(new PodcastsAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            if (mInterstitialAd.isLoaded()) {
                                mInterstitialAd.show();
                                id = podcasts.get(position).getId().toString();
                                title = podcasts.get(position).getTitle();
                                url = podcasts.get(position).getUrl();
                                imageUrl = podcasts.get(position).getImageUrl();
                                desc = podcasts.get(position).getDesc();
                            } else {
                                Log.d("TAG", "The interstitial wasn't loaded yet.");
                            }
                        }
                    });

                    rvPodcasts.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    isLoading = false;
                    adapter.refreshAdapter(isLoading,podcasts);
                    adapter.notifyDataSetChanged();
                } catch (Exception e){
                    //debug message and stacktrace
                    Log.d("onResponse", "There's an error");
                    e.printStackTrace();
                }
                loadingDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<Podcast>> call, Throwable t) {
                Log.e("Network fail get api", t.toString());
                Toast.makeText(getContext(),"No esta disponible este podcast, revisa tu conexión a Internet e intenta más tarde",Toast.LENGTH_LONG).show();
            }
        });
    }

    void loadAd() {
        AdRequest adRequest = new AdRequest.Builder()
                //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();

        // Load the adView object witht he request
        mInterstitialAd.loadAd(adRequest);
    }

    public void setPodcastsDataFirstTime(String page) throws Exception {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RestfulApiConstants.BASE_URL)
                .client(getSlrClubClient(getContext()))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PodcastService service = retrofit.create(PodcastService.class);
        Call<List<Podcast>> call = service.getJsonArrayData(page, RestfulApiConstants.TOKEN);

        call.enqueue(new Callback<List<Podcast>>() {
            @Override
            public void onResponse(Call<List<Podcast>> call, Response<List<Podcast>> response) {
                try{
                    for(int i = 0; i < response.body().size(); i++){
                        if (i % 2 == 0){
                            Podcast podForAd = new Podcast();
                            podForAd.setTitle("inlineAd");
                            podcasts.add(podForAd);
                        }
                        podcasts.add(response.body().get(i));
                    }
                    adapter = new PodcastsAdapter(getContext(),podcasts);

                    adapter.setOnItemClickListener(new PodcastsAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            id = podcasts.get(position).getId().toString();
                            title = podcasts.get(position).getTitle();
                            url = podcasts.get(position).getUrl();
                            imageUrl = podcasts.get(position).getImageUrl();
                            desc = podcasts.get(position).getDesc();
                            if (mInterstitialAd.isLoaded()) {
                                mInterstitialAd.show();
                            } else {
                                setupIntent();
                                getActivity().startActivity(podcastPlayerIntent);
                                Log.d("TAG", "The interstitial wasn't loaded yet.");
                            }
                        }
                    });
                    rvPodcasts.setAdapter(adapter);
                    firstLoadingBar.setVisibility(View.INVISIBLE); //Remove first loading indicator

                } catch (Exception e){
                    //debug message and stacktrace
                    Log.d("onResponse", "There's an error");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<Podcast>> call, Throwable t) {
                Log.e("Network fail get api", t.toString());
                Toast.makeText(getContext(),"No esta disponible este podcast, revisa tu conexión a Internet e intenta más tarde",Toast.LENGTH_LONG).show();
            }
        });
    }

    public Intent setupIntent()
    {
        podcastPlayerIntent = new Intent(getActivity(), PodcastPlayerActivity.class);
        podcastPlayerIntent.putExtra("id", id);
        podcastPlayerIntent.putExtra("title", title);
        podcastPlayerIntent.putExtra("url", url);
        podcastPlayerIntent.putExtra("imageUrl", imageUrl);
        podcastPlayerIntent.putExtra("desc",desc);
        podcastPlayerIntent.putExtra("segmentName",segmentName);

        return podcastPlayerIntent;
    }
}
